# Scenario:

Scenario name:  GSD-125_IUPAC_ref_alts

Description: '+' values in Alts properly loaded

GSD link: 
https://gobiiproject.atlassian.net/browse/GSD-125

## Flow:

load samples
load dataset
extract by dataset - flapjack
compare to known good extract
extract by dataset - hapmap
compare to known good extract
