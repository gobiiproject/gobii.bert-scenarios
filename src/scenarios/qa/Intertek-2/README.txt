# Scenario:

Scenario name:  Intertek_2ltr_nuc_dataset

Description: Load and extract Intertek 2 letter nucleotide dataset

GSD link: 
https://gobiiproject.atlassian.net/browse/GSD-165

## Flow:

load dataset
extract by dataset - flapjack
compare to known good extract
extract by dataset - hapmap
compare to known good extract
