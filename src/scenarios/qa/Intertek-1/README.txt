# Scenario:

Scenario name:  Intertek_2ltr_M49IBWSN

Description: Load and extract Intertek 2 letter nucleotide dataset

GSD link: 
https://gobiiproject.atlassian.net/browse/GSD-165
https://gobiiproject.atlassian.net/browse/GDM-864

## Flow:

load dataset
extract by dataset - flapjack
compare to known good extract
extract by dataset - hapmap
compare to known good extract
