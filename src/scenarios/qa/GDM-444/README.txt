# Scenario:

Scenario name: GDM-444_Tetraploid_Dataset_Type

Description:  User can upload a dataset type of 'nucleotide_4_letter' as per other data types.

GSD links:
https://gobiiproject.atlassian.net/browse/GDM-444

## Flow:

load markers
load samples
load dataset
extract by dataset - flapjack
compare to known good extract
extract by dataset - hapmap
compare to known good extract
