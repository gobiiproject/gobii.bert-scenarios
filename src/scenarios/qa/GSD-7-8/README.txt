# Scenario:

Scenario name: GSD-7-8_linkage_group_start-stop

Description: Linkage group start and stop and Marker Linkage group start and stop handles 3 decimal places

GSD links:
https://gobiiproject.atlassian.net/browse/GSD-7
https://gobiiproject.atlassian.net/browse/GSD-8

## Flow:

load dataset
extract by dataset - flapjack
compare to known good extract
extract by dataset - hapmap
compare to known good extract
